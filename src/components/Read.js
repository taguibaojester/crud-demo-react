import { Table, Button } from 'semantic-ui-react';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';


function Read() {
    const [APIData, setAPIData] = useState([]);
    useEffect(() => {
        axios.get(`https://655dc0479f1e1093c599cbdb.mockapi.io/api/v1/users`)
            .then((response) => {
                setAPIData(response.data);
            })
    }, [])

    const setData = (data) => {
        let { id, firstName, lastName, checkbox } = data;
        localStorage.setItem('ID', id);
        localStorage.setItem('First Name', firstName);
        localStorage.setItem('Last Name', lastName);
        localStorage.setItem('Checkbox Value', checkbox)
        console.log(data);
     }

     const onDelete = (id) => {
        axios.delete(`https://655dc0479f1e1093c599cbdb.mockapi.io/api/v1/users/${id}`)
        .then(() => {
            getData();
        })
        
     }

     const getData = () => {
        axios.get(`https://655dc0479f1e1093c599cbdb.mockapi.io/api/v1/users`)
            .then((getData) => {
                 setAPIData(getData.data);
             })
    }

    
  return (
    <div>
        <Table singleLine>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>ID</Table.HeaderCell>
                    <Table.HeaderCell>FirstName</Table.HeaderCell>
                    <Table.HeaderCell>LastName</Table.HeaderCell>
                    <Table.HeaderCell>Policy</Table.HeaderCell>
                    <Table.HeaderCell>Actions</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
            {APIData.map((data) => {
                return (
                <Table.Row>
                    <Table.Cell>{data.id}</Table.Cell>
                    <Table.Cell>{data.firstName}</Table.Cell>
                    <Table.Cell>{data.lastName}</Table.Cell>
                    <Table.Cell>{data.checkbox ? 'Checked' : 'Unchecked'}</Table.Cell>
                    <Link to='/update'>
                        <Table.Cell> 
                            <Button onClick={() => setData(data)}>Update</Button>
                        </Table.Cell>
                    </Link>
                    <Table.Cell>
                        <Button onClick={() => onDelete(data.id)}>Delete</Button>
                    </Table.Cell>
                </Table.Row>
            )})}
            </Table.Body>
        </Table>
    </div>
  )
}

export default Read